DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INT AUTO_INCREMENT NOT NULL,
  email NVARCHAR(320) NOT NULL,
  pwd VARCHAR(512) NOT NULL,
  fullName NVARCHAR(255),
  addr VARCHAR(255),
  phone VARCHAR(25),
  token VARCHAR(255),
  auth boolean DEFAULT false,
  PRIMARY KEY (`id`)
);
INSERT INTO users 
  (email, pwd, fullName, addr, phone) 
VALUES 
  ('test@mysite.com', '123', 'John Doe', 'Address Street 42', '42424242'),
  ('test2@mysite.com', '123', 'Michael Cera', 'Second Address Street 43', '123456789');
  