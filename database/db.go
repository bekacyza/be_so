// TODO
package database

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/go-sql-driver/mysql"
)

var DBConn *sql.DB

func InitDB() {
	config := mysql.Config{
		User:                 "root",
		Passwd:               "password",
		Net:                  "tcp",
		Addr:                 ":3306",
		DBName:               "goauth",
		AllowNativePasswords: true,
	}

	// Connect to DB
	var err error
	DBConn, err = sql.Open("mysql", config.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}

	pingErr := DBConn.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}
	fmt.Println("Database Connected!")

	createTable(DBConn)
}

func createTable(db *sql.DB) error {
	query := `CREATE TABLE IF NOT EXISTS users(
						id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
						email VARCHAR(320) NOT NULL,
						pwd VARCHAR(512) NOT NULL,
						fullName NVARCHAR(255) DEFAULT "",
						addr VARCHAR(255) DEFAULT "",
						phone VARCHAR(25) DEFAULT "",
						token VARCHAR(255) DEFAULT "",
						auth boolean DEFAULT false,
						oauth boolean DEFAULT false,
						newAccount boolean DEFAULT true)`
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	res, err := db.ExecContext(ctx, query)
	if err != nil {
		log.Printf("Error %s when creating users table", err)
		return err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		log.Printf("Error %s when getting rows affected", err)
		return err
	}
	log.Printf("Rows affected when creating table: %d", rows)
	return nil
}

func Close() {
	DBConn.Close()
}
