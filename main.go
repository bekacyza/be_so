package main

import (
	"be_so/database"
	"be_so/router"
)

func main() {
	database.InitDB()
	defer database.Close()
	router.Serve()
}
