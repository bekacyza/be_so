package models

type User struct {
	ID           int
	Email        string
	Password     string
	FullName     string
	Address      string
	Phone        string
	Token        string
	Auth         bool
	OAuth        bool
	NewAccount   bool
	ErrorMessage string
	State        string
}

type UserInfo struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	Email         string `json:"email"`
	EmailVerified string `json:"email_verified"`
}
