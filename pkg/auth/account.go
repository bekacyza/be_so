package auth

import (
	"be_so/database"
	"be_so/models"
	"be_so/pkg/session"
	"fmt"
	"net/http"
)

func Account(w http.ResponseWriter, r *http.Request) {
	s := session.InitSession(r)
	userSession := GetSession(s)
	auth := userSession.Auth
	u := models.User{}
	u.Email = userSession.Email

	if !auth {
		s.AddFlash("No Access!")
		err := s.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	err := database.DBConn.QueryRow("SELECT email, fullName, addr, phone, auth FROM users WHERE email=?", u.Email).Scan(&u.Email, &u.FullName, &u.Address, &u.Phone, &u.Auth)
	if err != nil {
		fmt.Println(err)
	}

	GetUser(u.Email)
	s.Values["user"] = u
	err = s.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	t.ExecuteTemplate(w, "index.html", u)
}

func AccountSettings(w http.ResponseWriter, r *http.Request) {
	s := session.InitSession(r)
	userSession := GetSession(s)
	u := models.User{}
	u.Auth = userSession.Auth
	u.Email = userSession.Email

	err := database.DBConn.QueryRow("SELECT id, email, fullName, addr, phone, oauth, newAccount FROM users WHERE email=?", u.Email).Scan(&u.ID, &u.Email, &u.FullName, &u.Address, &u.Phone, &u.OAuth, &u.NewAccount)
	if err != nil {
		fmt.Println(err)
	}

	if r.Method == "POST" {
		email := r.FormValue("email")
		fullName := r.FormValue("fullName")
		phone := r.FormValue("phone")
		addr := r.FormValue("addr")

		_, err = database.DBConn.Exec("UPDATE users SET email = ?, fullName = ?, addr = ?, phone = ?, newAccount = ? WHERE id = ?", email, fullName, addr, phone, false, u.ID)
		if err != nil {
			fmt.Println(err)
			http.Error(w, "Server error, unable save account details!", http.StatusInternalServerError)
			return
		}

		err := database.DBConn.QueryRow("SELECT email, fullName, addr, phone FROM users WHERE id=?", u.ID).Scan(&u.Email, &u.FullName, &u.Address, &u.Phone)
		if err != nil {
			fmt.Println(err)
		}

		s.Values["user"] = u
		err = s.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Println("Successfully Changed Account Details!")
		t.ExecuteTemplate(w, "index.html", u)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	} else {
		if u.Auth {
			http.Redirect(w, r, "/account-settings", 200)
			t.ExecuteTemplate(w, "account-settings.html", u)
			return
		} else {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
	}
}
