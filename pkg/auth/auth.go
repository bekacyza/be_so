package auth

import (
	"be_so/database"
	"be_so/models"
	"be_so/pkg/session"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"net/http"
	"text/template"

	"github.com/gorilla/sessions"
)

var store *sessions.CookieStore
var t *template.Template

func init() {
	t = template.Must(template.ParseGlob("src/templates/*.html"))
}

func GetSession(s *sessions.Session) models.User {
	v := s.Values["user"]
	u := models.User{}
	u, ok := v.(models.User)
	if !ok {
		return models.User{Auth: false}
	}

	return u
}

func Index(w http.ResponseWriter, r *http.Request) {
	s := session.InitSession(r)
	u := GetSession(s)

	err := s.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if u.Auth {
		http.Redirect(w, r, "/account", http.StatusFound)
		t.ExecuteTemplate(w, "index.html", u)
	} else {
		t.ExecuteTemplate(w, "login.html", u)
	}
}

func RandomCharGenerator(len int) string {
	b := make([]byte, len)
	rand.Read(b)
	s := base64.URLEncoding.EncodeToString(b)
	return s
}

func GetUser(email string) (u models.User, err error) {
	u = models.User{}

	rows, err := database.DBConn.Query("SELECT * FROM users WHERE email = ?", email)
	if err != nil {
		fmt.Println(err.Error())
	}

	for rows.Next() {
		err = rows.Scan(&u.ID, &u.Email, &u.Password, &u.FullName, &u.Address, &u.Phone, &u.Token, &u.Auth, &u.OAuth, &u.NewAccount)
	}

	return u, err
}
