package auth

import (
	"be_so/database"
	"be_so/models"
	"be_so/pkg/session"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var config *oauth2.Config
var clientID = "42798272404-clb7upb8gbbnshneomu7up30qr9nkto6.apps.googleusercontent.com"
var clientSecret = "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"
var sessionToken = ""

func init() {
	authKey := securecookie.GenerateRandomKey(64)
	encryptKey := securecookie.GenerateRandomKey(32)
	store = sessions.NewCookieStore(authKey, encryptKey)

	store.Options = &sessions.Options{
		Path:     "/oauth",
		HttpOnly: true,
		MaxAge:   360,
	}

	config = &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURL:  "http://localhost:8080/oauth",
		Endpoint:     google.Endpoint,
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
	}
}

func loginURL(state string) string {
	return config.AuthCodeURL(state)
}

func GoogleLoginURL(w http.ResponseWriter, r *http.Request) {
	state := RandomCharGenerator(16)
	s := session.InitSession(r)
	u := GetSession(s)
	u.Token, sessionToken = state, state
	s.Values["user"] = models.User{}

	err := s.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, loginURL(state), http.StatusTemporaryRedirect)
}

func GoogleAuth(w http.ResponseWriter, r *http.Request) {
	s := session.InitSession(r)
	userSession := GetSession(s)
	userSession.Auth = true
	s.Values["user"] = models.User{}

	if r.FormValue("state") != sessionToken {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	tok, err := config.Exchange(context.Background(), r.FormValue("code"))
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + url.QueryEscape(tok.AccessToken))
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer resp.Body.Close()
	data, _ := ioutil.ReadAll(resp.Body)

	uInfo := models.UserInfo{}
	json.Unmarshal([]byte(data), &uInfo)
	u := &models.User{}
	u.Email = uInfo.Email
	u.FullName = uInfo.Name
	u.Password = RandomCharGenerator(16)
	u.Auth = true

	dataHandler(u, w, r)
}

func dataHandler(u *models.User, w http.ResponseWriter, r *http.Request) {
	s := session.InitSession(r)
	// u := models.User{}
	userSession := GetSession(s)
	s.Values["user"] = u

	row := database.DBConn.QueryRow("SELECT email FROM users WHERE email=?", u.Email).Scan(&u.Email)

	switch {
	case row == sql.ErrNoRows:
		_, row = database.DBConn.Exec("INSERT INTO users(email, pwd, fullName, auth, oauth) VALUES(?, ?, ?, ?, ?)", u.Email, u.Password, u.FullName, u.Auth, true)
		if row != nil {
			fmt.Println(row)
			http.Error(w, "Server error, unable to create account!", http.StatusInternalServerError)
		}
		userSession.Auth = true
		err := s.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/account-settings", http.StatusFound)
		t.ExecuteTemplate(w, "account-settings.html", u)
		return
	case row != sql.ErrNoRows:
		_, err := database.DBConn.Exec("UPDATE users SET auth = ? WHERE email = ?", u.Auth, u.Email)
		if err != nil {
			fmt.Println(err)
			http.Error(w, "Server error, unable save account details!", http.StatusInternalServerError)
		}

		userSession.Auth = true
		err = s.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		s := session.InitSession(r)
		userSession.Auth = true
		err = s.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/account", http.StatusFound)
		t.ExecuteTemplate(w, "account.html", u)
		return
	case row != nil:
		http.Error(w, "Server error, unable to create account!", http.StatusInternalServerError)
		return
	}
}
