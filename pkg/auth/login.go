package auth

import (
	"be_so/database"
	"be_so/models"
	"be_so/pkg/session"
	"database/sql"
	"fmt"
	"net/http"
)

func Login(w http.ResponseWriter, r *http.Request) {
	s := session.InitSession(r)

	if r.Method == "POST" {
		u := models.User{}
		r.ParseForm()
		email := r.Form["email"][0]
		pwd := r.Form["pwd"][0]
		u.Email = email

		err := database.DBConn.QueryRow("SELECT id, email, pwd, fullName, addr, phone FROM users WHERE email = ?", email).Scan(&u.ID, &u.Email, &u.Password, &u.FullName, &u.Address, &u.Phone)
		if err != nil {
			fmt.Println(err.Error())
		}
		if err == sql.ErrNoRows {
			u.ErrorMessage = "Email isn't registered!"
			t.ExecuteTemplate(w, "login.html", u)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		u.ErrorMessage = loginErrorHandler(w, email, pwd, u)
		if u.ErrorMessage != "" {
			t.ExecuteTemplate(w, "login.html", u)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		if email == u.Email && pwd == u.Password {
			u.Auth = true
			s.Values["user"] = u

			_, err = database.DBConn.Exec("UPDATE users SET auth = ? WHERE email = ?", u.Auth, email)
			if err != nil {
				fmt.Println(err)
			}

			err = s.Save(r, w)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			http.Redirect(w, r, "/account", http.StatusFound)
			return
		}
	} else {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
}

func loginErrorHandler(w http.ResponseWriter, email, pwd string, u models.User) string {
	switch {
	case (u.Email == "" || pwd == ""):
		u.ErrorMessage = "All fields are required!"
	case (u.Email == "" && pwd == ""):
		u.ErrorMessage = "Input fields are empty!"
	case (email == u.Email && pwd != u.Password):
		u.ErrorMessage = "Wrong password!"
	}

	return u.ErrorMessage
}
