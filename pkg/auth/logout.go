package auth

import (
	"be_so/database"
	"be_so/models"
	"be_so/pkg/session"
	"fmt"
	"net/http"
)

func LogOut(w http.ResponseWriter, r *http.Request) {
	s := session.InitSession(r)
	g := GetSession(s)
	u := models.User{}
	u.Auth = false
	s.Values["user"] = models.User{}
	s.Options.MaxAge = -1

	err := s.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = database.DBConn.Exec("UPDATE users SET auth = ? WHERE email = ?", false, g.Email)
	if err != nil {
		fmt.Println(err)
	}

	http.Redirect(w, r, "/", http.StatusFound)
}
