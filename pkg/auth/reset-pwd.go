package auth

import (
	"be_so/database"
	"be_so/models"
	"database/sql"
	"fmt"
	"net/http"
	"net/smtp"
	"time"
)

var token = ""

func SendEmail(w http.ResponseWriter, r *http.Request) {
	u := models.User{}
	e := models.Email{}
	u.Email = r.FormValue("email")
	e.To = []string{u.Email}
	token = RandomCharGenerator(16)
	// Sender data.
	e.From = "beka.void@gmail.com"
	e.Password = "goAuth123"
	e.Subject = "Recover Your Password"
	p := "http://localhost:8080"
	url := fmt.Sprintf("%s/reset-password?token=%s", p, token)

	if r.Method == "POST" {
		row := database.DBConn.QueryRow("SELECT email FROM users WHERE email=?", u.Email).Scan(&u.Email)

		u.ErrorMessage = emailErrorHandler(w, u, row)
		if u.ErrorMessage != "" {
			t.ExecuteTemplate(w, "forgot-password.html", u)
			return
		}

		if row != sql.ErrNoRows {
			storeToken(u.Email, token)
			u.ErrorMessage = "Password recovery link was sent to " + u.Email

			// smtp server configuration.
			smtpHost := "smtp.gmail.com"
			smtpPort := "587"

			// Message.
			message := "From: " + e.From +
				"\r\nTo: " + u.Email +
				"\r\nSubject: Recover Your Password\r\n\r\n" +
				"Follow the URL to recover your password: " + url + "\r\n"

			// Authentication.
			auth := smtp.PlainAuth("", e.From, e.Password, smtpHost)

			// Sending email.
			err := smtp.SendMail(smtpHost+":"+smtpPort, auth, e.From, e.To, []byte(message))
			if err != nil {
				fmt.Println("Couldn't send the email!")
				fmt.Println(err)
				return
			}

			t.ExecuteTemplate(w, "forgot-password.html", u)
			return
		}
	} else {
		if u.Auth {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		} else {
			t.ExecuteTemplate(w, "forgot-password.html", u)
			return
		}
	}
}

func emailErrorHandler(w http.ResponseWriter, u models.User, row error) string {
	switch {
	case (u.Email == ""):
		u.ErrorMessage = "Email is required!"
	case row == sql.ErrNoRows:
		u.ErrorMessage = "The email (" + u.Email + ") you provided isn't registered!"
	}

	return u.ErrorMessage
}

func storeToken(email, token string) {
	_, err := database.DBConn.Exec("UPDATE users SET token = ?, auth = ? WHERE email = ?", token, false, email)
	if err != nil {
		fmt.Println(err)
	}

	u := models.User{}
	GetUser(email)

	row := database.DBConn.QueryRow("SELECT email FROM users WHERE token=?", token).Scan(&u.Token)
	if row != sql.ErrNoRows {
		time.AfterFunc(15*time.Minute, func() {
			_, err := database.DBConn.Exec("UPDATE users SET token = ? WHERE email = ?", "", email)
			if err != nil {
				fmt.Println(err)
			}
			println("Removed token after 15 min!")
		})
	}
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	u := models.User{}
	pwd := r.FormValue("pwd")
	pwdRepeat := r.FormValue("pwdRepeat")

	if r.Method == "POST" {
		row := database.DBConn.QueryRow("SELECT token FROM users WHERE token=?", token).Scan(&u.Token)

		u.ErrorMessage = pwdErrorHandler(w, u, pwd, pwdRepeat)
		if u.ErrorMessage != "" {
			t.ExecuteTemplate(w, "reset-password.html", u)
			return
		}

		if row != sql.ErrNoRows {
			_, err := database.DBConn.Exec("UPDATE users SET pwd = ?, token = ?, auth = ? WHERE token = ?", pwd, "", false, token)
			if err != nil {
				fmt.Println(err)
			}

			u.ErrorMessage = "Your password has been updated successfully! Go to login page to sign in."
		}

		t.ExecuteTemplate(w, "reset-password.html", u)
		return
	} else {
		if u.Auth {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		} else {
			t.ExecuteTemplate(w, "reset-password.html", u)
			return
		}
	}
}

func pwdErrorHandler(w http.ResponseWriter, u models.User, pwd string, pwdRepeat string) string {
	switch {
	case (pwd == "" || pwdRepeat == ""):
		u.ErrorMessage = "All fields are required!"
	case (pwd != pwdRepeat):
		u.ErrorMessage = "Passwords don't match!"
	case (token != u.Token):
		u.ErrorMessage = "Password reset link has expired!"
	}

	return u.ErrorMessage
}
