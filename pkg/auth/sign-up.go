package auth

import (
	"be_so/database"
	"be_so/models"
	"be_so/pkg/session"
	"database/sql"
	"net/http"
)

func SignUp(w http.ResponseWriter, r *http.Request) {
	email := r.FormValue("email")
	pwd := r.FormValue("pwd")
	pwdRepeat := r.FormValue("pwdRepeat")
	u := models.User{}

	if r.Method == "POST" {
		err := database.DBConn.QueryRow("SELECT email FROM users WHERE email=?", email).Scan(&u.Email)
		u.Email = email
		u.Password = pwd
		u.ErrorMessage = signUpErrorHandler(w, email, pwd, pwdRepeat, u)
		if u.ErrorMessage != "" {
			t.ExecuteTemplate(w, "sign-up.html", u)
			return
		}

		switch {
		case err == sql.ErrNoRows:
			_, err = database.DBConn.Exec("INSERT INTO users(email, pwd) VALUES(?, ?)", email, pwd)
			if err != nil {
				http.Error(w, "Server error, unable to create account!", http.StatusInternalServerError)
			}

			s := session.InitSession(r)
			u.Auth = true
			u.NewAccount = true
			s.Values["user"] = u

			err = s.Save(r, w)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			http.Redirect(w, r, "/account-settings", http.StatusSeeOther)
			t.ExecuteTemplate(w, "account-settings.html", u)
			return
		case err != sql.ErrNoRows:
			u.ErrorMessage = "Couldn't create an account! Email is already registered."
			t.ExecuteTemplate(w, "sign-up.html", u)
		case err != nil:
			http.Error(w, "Server error, unable to create account!", http.StatusInternalServerError)
		}
	} else {
		if u.Auth {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		} else {
			http.Redirect(w, r, "/sign-up", 200)
			t.ExecuteTemplate(w, "sign-up.html", u)
			return
		}
	}
}

func signUpErrorHandler(w http.ResponseWriter, email, pwd, pwdRepeat string, u models.User) string {
	switch {
	case (email == "" || pwd == "" || pwdRepeat == ""):
		u.ErrorMessage = "All fields are required!"
	case (email == ""):
		u.ErrorMessage = "Email is required!"
	case (pwd != pwdRepeat):
		u.ErrorMessage = "Passwords do not match!"
	}

	return u.ErrorMessage
}
