package session

import (
	"be_so/models"
	"encoding/gob"
	"fmt"
	"net/http"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
)

var authKey = securecookie.GenerateRandomKey(64)
var encryptKey = securecookie.GenerateRandomKey(32)
var store = sessions.NewCookieStore(authKey, encryptKey)

func InitSession(r *http.Request) *sessions.Session {
	s, err := store.Get(r, "goauth")
	if err != nil {
		fmt.Println(err)
	}

	if s.IsNew {
		s.Options.Domain = "localhost"
		s.Options.MaxAge = 60 ^ 2
		s.Options.HttpOnly = true
		s.Options.Secure = true
	}

	gob.Register(models.User{})

	// fmt.Println("SESSION SET!!!")

	return s
}
