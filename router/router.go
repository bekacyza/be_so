package router

import (
	"be_so/pkg/auth"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

func Serve() {
	r := mux.NewRouter()
	p := os.Getenv("PORT")
	if p == "" {
		p = ":8080"
	}
	fs := http.FileServer(http.Dir("./src"))
	r.PathPrefix("/src/").Handler(http.StripPrefix("/src/", fs))

	r.HandleFunc("/", auth.Index)
	r.HandleFunc("/sign-up", auth.SignUp)
	r.HandleFunc("/login", auth.Login)
	r.HandleFunc("/logout", auth.LogOut)
	r.HandleFunc("/account", auth.Account)
	r.HandleFunc("/account-settings", auth.AccountSettings)
	r.HandleFunc("/oauth", auth.GoogleAuth)
	r.HandleFunc("/oauth/google", auth.GoogleLoginURL)
	r.HandleFunc("/forgot-password", auth.SendEmail)
	r.HandleFunc("/reset-password", auth.ResetPassword)

	srv := &http.Server{
		Handler:      r,
		Addr:         p,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Listening On: ", p)
	log.Fatal(srv.ListenAndServe())
}
